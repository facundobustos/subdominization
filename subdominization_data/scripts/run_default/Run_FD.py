#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys


def build_task_list(run_path):
	task_list_file = open(os.path.join(run_path,"task_list"), 'r')
	task_list = []
	for linea in task_list_file.readlines():
		if not linea.startswith("#"):
			task = linea.split()[0]
			task_list.append(task)			
	task_list_file.close() 
	return task_list


def build_directories_per_task(task_list, run_path):
	for task in task_list:
		os.mkdir(os.path.join(run_path, task))


def copy_RunFD_script_per_task(task_list, run_path):
	for task in task_list:
		shutil.copy("Run_FD_per_task.py", os.path.join(run_path, task, "Run_FD_per_task.py"))


def copy_domain_per_task(task_list, run_path, domain_path):
	for task in task_list:
		shutil.copy(domain_path, os.path.join(run_path, task, "domain.pddl"))

def copy_problem_per_task(task_list, run_path, tasks_path):
	for task in task_list:
		shutil.copy(os.path.join(tasks_path, task), os.path.join(run_path, task, "problem.pddl"))


def run_task(task, run_path):
	os.chdir(os.path.join(run_path, task))
	os.system("nohup ./Run_FD_per_task.py " + task + " " +  fast_downward_path + " " + alias + " " + overall_time_limit + " " + overall_memory_limit + " &")			




if __name__ == "__main__":
	############ configurable parameters ############
	bench = "/home/fbustos/Documents/planning/subdominization/subdominization_data"
	domain = "depots"
	run_folder = "runs/satisfacing"
	tasks_folder = "instances"


	fast_downward_path = "/home/fbustos/Documents/planning/fast-downward/downward/fast-downward.py"
	alias = "lama-first"
	overall_time_limit = "30m" 
	overall_memory_limit = "4G"

	############  ############

	run_path = os.path.join(bench,domain,run_folder)
	tasks_path = os.path.join(bench,domain,tasks_folder)
	domain_path = os.path.join(bench,domain,"domain.pddl")

	task_list = build_task_list(run_path)
	build_directories_per_task(task_list, run_path)

	copy_RunFD_script_per_task(task_list, run_path)
	copy_domain_per_task(task_list, run_path, domain_path)
	copy_problem_per_task(task_list, run_path, tasks_path)
	
	for task in task_list:
		print(task)	
		run_task(task, run_path)
		#break
		




