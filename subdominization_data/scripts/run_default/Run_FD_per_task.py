#!/usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys


if __name__ == "__main__":

	fast_downward_path = sys.argv[2]
	alias = sys.argv[3]
	overall_time_limit= sys.argv[4]
	overall_memory_limit = sys.argv[5]

	print("planner: " + fast_downward_path)
	print("alias:" + alias)
	print("overall_time_limit: " + overall_time_limit)
	print("overall_memory_limit: " + overall_memory_limit)


	log = subprocess.run([fast_downward_path,"--overall-time-limit", overall_time_limit, "--overall-memory-limit", overall_memory_limit, "--alias", alias, "--keep-sas-file", "domain.pddl", "problem.pddl"], capture_output=True, text=True)

	f = open("run.log","w")
	f.write(log.stdout)
	f.close()

	print("Done.")

