#! /usr/bin/env python

#Command line
#./recover_incremental_data.py <bench> <domain> <run_folder>
#Example
#./recover_incremental_data.py /home/fbustos/planning/subdominization/subdominization_data satellite runs/incremental/subdom-med_LOGR_optimal

import os
import argparse
import sys


if __name__ == "__main__":
	#configurable parameters

#	bench = "/home/fbustos/planning/subdominization/subdominization_data"
	bench = sys.argv[1]

#	domain = "satellite"
	domain = sys.argv[2]

#	run_folder = "runs/incremental/subdom-med_LOGR_optimal"
	run_folder = sys.argv[3]

	output_file_name = "incremental_data.csv"

	#patterns to find
	pattern_0 = "Solution found!"		
#	pattern_1 = "Translator operators:"
	pattern_2 = "actions instantiated"
	pattern_3 = "incremental grounding successfull after"
	pattern_4 = "Translator ran out of memory"	
	pattern_5 = "incremental grounding ran out of time"
	pattern_6 = "translate exit code: 21"
	

	f = open (os.path.join(bench,domain,run_folder, output_file_name),'w')
	entries = os.listdir(os.path.join(bench,domain,run_folder))	
	entries.sort()	
	for entry in entries:
		if os.path.isdir(os.path.join(bench,domain,run_folder, entry)):
			try:
				fl = open(os.path.join(bench, domain, run_folder, entry,"run.log"),"r")
				esg_t = "0"
				esg_h = "0"
				sat = False
				iteration = 0
				resources_out = ""
				for line in fl:
					if pattern_0 in line:
						sat = True		
#					if pattern_1 in line:
#						esg_t = line.split()[-1]
					if pattern_2 in line:
						iteration = iteration + 1
						esg_h = line.split()[0]
					if pattern_3 in line:
						time = line.split()[-1]
						time = time[0:len(time)-2]
					if pattern_4 in line:
						resources_out = "MO"
					if pattern_5 in line or pattern_6 in line:
						resources_out = "TO"
			
				if sat == True:
					f.write(entry + "," + esg_h + "," + str(iteration) + "," + "T" + "," + time + "\n")
				else:
					f.write(entry + "," + esg_h + "," + str(iteration) + "," + "F" + "," + resources_out + "\n")

			except:
				continue
	f.close()
			 
