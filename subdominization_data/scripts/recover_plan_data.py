#! /usr/bin/env python

#Command line
#./recover_plan_data.py <bench> <domain> <run_folder>
#Example
#./recover_plan_data.py /home/fbustos/planning/subdominization/subdominization_data satellite runs/incremental/subdom-med_LOGR_optimal

import os
import argparse
import sys


if __name__ == "__main__":
	#configurable parameters

#	bench = "/home/fbustos/planning/subdominization/subdominization_data"
	bench = sys.argv[1]

#	domain = "satellite"
	domain = sys.argv[2]

#	run_folder = "runs/incremental/subdom-med_LOGR_optimal"
	run_folder = sys.argv[3]

	length = {}
	entries = os.listdir(os.path.join(bench,domain,run_folder))	
	entries.sort()	
	for entry in entries:
		if os.path.isdir(os.path.join(bench,domain,run_folder, entry)):
			try:
				fl = open(os.path.join(bench, domain, run_folder, entry,"sas_plan"),"r")
				plan_length = 0
				for line in fl:
					if not line.startswith(";"):
						plan_length+= 1
				fl.close()
				length[entry] = plan_length
			except:
				continue

	print("Average plan length: ", sum(length.values())/len(length))
				 
