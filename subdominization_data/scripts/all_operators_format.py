#! /usr/bin/env python


import os
import argparse
import sys

import bz2

if __name__ == "__main__":
	#configurable parameters
	bench = "/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/"
	domain = "blocksworld"
	run_folder = "runs/proof"

	task_list = os.listdir(os.path.join(bench,domain,run_folder))	
	for task in task_list:

		#first: extract the operator from output.sas
		try:
			#print("Proccesing " + task)
			f = open(os.path.join(bench, domain, run_folder, task, "output.sas"),"r")
			print(task)
			all_operator = []
			flag = False	
			for line in f:
				if flag:
					all_operator.append(line.strip())
					flag = False
				if line.startswith("begin_operator"):
					flag = True
			f.close()
		except:
			continue


#		#second: format de operator op o1 ... on -> op(o1,...,on)
#		f = open(os.path.join(bench, domain, run_folder, task, "all_operators"),"w")
#		for op in all_operator:
#			tokens = op.split()
#			arity = len(tokens) -1 
#			action = tokens[0] + "("
#			for i in range(1, arity+1):
#				if i == arity: 
#					action+= tokens[i] + ")\n"
#				else:
#					action+= tokens[i] + ","
#			
#			f.write(action)
#		f.close()

		#third: compress to bz2
		with bz2.open("myfile.bz2", "wb") as f:
			f.write(all_operator) # Write compressed data to file






