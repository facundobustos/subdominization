#!/usr/bin/env python

import sys
import os
import numpy as np
import pandas as pd


#import seaborn as sb
from sklearn.pipeline import make_pipeline
import matplotlib.pyplot as plt
import sklearn
from sklearn import tree
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import GaussianNB
from sklearn.base import BaseEstimator
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
#from pandas import Series, DataFrame
from pylab import rcParams
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics 
from sklearn.metrics import classification_report

from sklearn.linear_model import LogisticRegression
from sklearn import datasets
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegressionCV
#from sklearn.cross_validation import KFold
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor

from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import fbeta_score
from sklearn.metrics import classification_report
#from optparse import isbasestring
import pickle
from random import *
from sklearn.metrics import r2_score
from sklearn.metrics import make_scorer 
from sklearn.svm import SVR
from sklearn.kernel_ridge import KernelRidge
from sklearn.decomposition import PCA

from sklearn import datasets, linear_model
# Importamos la clase de Regresión Lineal de scikit-learn
from sklearn.linear_model import LinearRegression 
# para generar características polinómicas
from sklearn.preprocessing import PolynomialFeatures 

#from __builtin__ import None
randBinList = lambda n: [randint(0,1) for b in range(1,n+1)]


#CLASS_SUFIX=".classifier"

class LearnRules():
    model=BaseEstimator
    X_test=pd.DataFrame()
    y_test=pd.DataFrame()
    isBalanced=''
    tesSize=0.05
    njobs=4
    modelType=''

    #X_train, y_train are not defined, to not save with pickle
    
    def __init__(self, isBalanced=False, modelType='LRCV', fileTraining ='', njobs=-1, testSize=0.05):
        '''
        Constructor take parameters:
        isBalanced, Boolean for balance the target of prediction in training phase
        modelType = 'LRCV', 'LG', 'RF' , 'SVMCV','NBB', 'NBG'. 'DT'; 
                       Logistic Regression, 
                       Logistic Regression with Cross VAlidation, 
                       Random Forest, 
                       Support Vector MAchine with CV grid search,
                       Naive Bayes classifier with Bernoulli estimator
                       Naive Bayes classifier with Gaussian estimator 
                       DT is decision Tree 
        you ahve to give:
                  'fileTraining' that is a CSV file containing in each line the feature vectors (validation of rules), and the las column the target to be predicted
                  
        njobs, to paralellice the training phase, default njobs=-1 to get the availables cores
        testSize, is to define the size of test set. Default value calculates the test set random, with a 5% of the training set.
        '''
        
        self.tesSize=testSize
        self.njobs=njobs
        self.modelType=modelType
        
        if (fileTraining !='') :
            self.isBalanced=  'balanced' if isBalanced else None
            print (self.isBalanced)
            dataset= pd.read_csv(fileTraining, header=None)
            #print dataset.shape
            # separate in features an target
            X, y = dataset.iloc[:,:-1], list(dataset.iloc[:, -1])
    
            #if we want to saparate in train an test
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=self.tesSize, random_state=0)
            self.y_test=y_test
            self.X_test=X_test
           
    
            X=X_train
            y=y_train
    
            # Standarize features
            scaler = StandardScaler(with_std=True)
    
            X_std = scaler.fit_transform(X)
            
            
            if (modelType=='LR'):
                # Create decision tree classifer object
                clf = LogisticRegression(random_state=0
                         ,class_weight=self.isBalanced
                         )
                self.model = clf.fit(X_std, y)
                #model_simple = clf.fit(X, y)
                print(np.std(X_std, 0)*self.model.coef_)
            elif (modelType=='LRCV'):
                
                #fold = KFold(len(y), n_folds=20, shuffle=True, random_state=1)
                fold = KFold(n_splits=20, shuffle=True, random_state=1)
                searchCV = LogisticRegressionCV(
                Cs=list(np.power(10.0, np.arange(-10, 10)))
                    ,penalty='l2'
                    ,scoring='roc_auc'
                    ,cv=fold
                    ,random_state=777
                    ,max_iter=10000
                    ,fit_intercept=True
                    ,solver='newton-cg'
                    ,tol=10
                    ,class_weight=self.isBalanced
                    ,n_jobs=self.njobs
                    )

                self.model =searchCV.fit(X_std , y)
                print ("the magnitude of this matrix, give an idea of the featres influences")
                print(np.std(X_std, 0)*self.model.coef_)

                
            elif (modelType=='RF'):
                clf_RG = RandomForestClassifier(n_jobs=self.njobs, random_state=0,class_weight=self.isBalanced)
                self.model = clf_RG.fit(X_std, y)   
                
            elif (modelType=='SVMCV'):
                classifier_pipeline=None
                #tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                #     'C': [1, 10, 100, 1000]},
                #    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
                params={'kernel':['linear', 'rbf', 'poly', 'sigmoid'],
                    'C':[1, 5, 10],
                    'degree':[2,3],
                    'gamma':[0.025, 1.0, 1.25, 1.5, 1.75, 2.0],
                    'coef0':[2, 5, 9],
                    'class_weight': [ self.isBalanced]}
                
                clf = GridSearchCV(SVC(), params, cv=3,
                       scoring='roc_auc',n_jobs=self.njobs)
                self.model =clf.fit(X_std , y)
                #print self.model.predict_proba(self.X_test)
            elif (modelType=='SVM'):
                clf = SVC( class_weight=self.isBalanced)     
                self.model =clf.fit(X_std , y)
                #print self.model.predict_proba(self.X_test)

            elif (modelType=='NBB'):
                # Create decision tree classifer object
                clf= None
                if (not self.isBalanced):
                    clf = BernoulliNB()
                else:
                    clf= make_pipeline(scaler,BernoulliNB())
                self.model = clf.fit(X_std, y)
            elif (modelType=='NBG'):
                # Create decision tree classifer object
               
                
                clf = GaussianNB()
                self.model = clf.fit(X_std, y)
            elif (modelType=='DT'):

                clf  = tree.DecisionTreeClassifier(class_weight=self.isBalanced)
                self.model  = clf.fit(X_std, y)

            elif (modelType=='DT_RG'):
                clf  = tree.DecisionTreeRegressor()
                self.model  = clf.fit(X_std, y)
                       
            elif (modelType=='DTGD_RG'):
                scoring = make_scorer(r2_score)
                clf  = GridSearchCV( tree.DecisionTreeRegressor(random_state=0),
                param_grid={'min_samples_split': range(2, 10)}, scoring=scoring, cv=5, refit=True)
                self.model  = clf.fit(X_std, y)
            elif (modelType=='RF_RG'):
                clf  = RandomForestRegressor()
                self.model  = clf.fit(X_std, y)
            elif (modelType=='RFGD_RG'):
                param_grid = { 
                "n_estimators"      : [10,20,30],
                "max_features"      : ["auto", "sqrt", "log2"],
                "min_samples_split" : [2,4,8],
                "bootstrap": [True, False],
                }
                clf  = GridSearchCV(RandomForestRegressor(), param_grid, n_jobs=-1, cv=5)
                self.model  = clf.fit(X_std, y)
            elif (modelType=='SVMCV_RG'):
                classifier_pipeline=None
                #tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                #     'C': [1, 10, 100, 1000]},
                #    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
                params={'kernel':['linear', 'rbf', 'poly', 'sigmoid'],
                    'C':[1, 5, 10],
                    'degree':[2,3],
                    'gamma':[0.025, 1.0, 1.25, 1.5, 1.75, 2.0],
                    'coef0':[2, 5, 9],
                    }
                
                clf = GridSearchCV(SVR(), params, cv=3,
                       scoring='roc_auc',n_jobs=self.njobs)
                self.model =clf.fit(X_std , y)
                #print self.model.predict_proba(self.X_test)
            elif (modelType=='SVM_RG'):
                clf = SVR()     
                self.model =clf.fit(X_std , y)
                #print self.model.predict_proba(self.X_test)
            elif (modelType=='KRNCV_RG'):
                classifier_pipeline=None
                #tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                #     'C': [1, 10, 100, 1000]},
                #    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
                params={'kernel':['linear', 'rbf', 'poly', 'sigmoid'],
                    'C':[1, 5, 10],
                    'degree':[2,3],
                    'gamma':[0.025, 1.0, 1.25, 1.5, 1.75, 2.0],
                    'coef0':[2, 5, 9],
                    }
                
                clf = GridSearchCV(KernelRidge(), params, cv=3,
                       scoring='roc_auc',n_jobs=self.njobs)
                self.model =clf.fit(X_std , y)
                #print self.model.predict_proba(self.X_test)
            elif (modelType=='KRN_RG'):
                clf = KernelRidge()     
                self.model =clf.fit(X_std , y)
                #print self.model.predict_proba(self.X_test)            
            elif (modelType=='LN_RG'):
                clf = linear_model.LinearRegression()     
                self.model =clf.fit(X , y)
                #print(X)
                #print(y)
                print(clf.coef_[0])
                print(clf.intercept_)
                #print self.model.predict_proba(self.X_test)            
            elif (modelType=='PL3_RG'):
                pf = PolynomialFeatures(degree = 3)
                X_pol = pf.fit_transform(X_std)
                clf = linear_model.LinearRegression()     
                self.model =clf.fit(X_pol , y)
                 #print self.model.predict_proba(self.X_test)            
            
            
            else:
                SyntaxError("Error in modelType = 'LRCV', 'LG', 'RF', 'SVM', 'SVMCV', 'NBB', 'NBG' , 'DT'; \nLogistic Regression, Logistic Regression with Cross Validation, \nRandom Forest or Support Vector Machine with CV, \n DT  is decision Tree ")      
#        elif (fileModel!= ''):
#            self=pickle.load(open(fileModel, 'rb'))  
            
          
            
        else:
            SystemError("fileTrain should not be empty.")  

    def returnProbClasesList(self,X_t):
        
        
        return self.model.predict_proba(X_t)
      
 
    def returnProbClasesOne(self,x_t):
        if self.model.probability:
            return self.model.predict_proba([x_t])
        else:
            return None
            
    def returnClasesList(self,X_t):
        
        return self.model.predict(X_t)
 
    def returnClasesOne(self,x_t):
        return self.model.predict([x_t])

    def printStats(self):
        y_pred=None
        if not( self.modelType.endswith('RG')):
            y_pred= self.model.predict_proba(self.X_test)
        y_predClass= self.model.predict(self.X_test)
        if not( self.modelType.endswith('RG')):
            print (classification_report(list(self.y_test), y_predClass ))
            print  (confusion_matrix(self.y_test, y_predClass))
        
        if self.modelType.startswith('LR'):
            print ("Coefficient matrix")
            print (self.model.coef_)
        elif (self.modelType.startswith('RF') or self.modelType.startswith('DT')):
            print (self.model.feature_importances_)
        elif (self.modelType.startswith('SV') or self.modelType.startswith('DT')):
            print (self.model.coef_)
        #
    def saveToDisk(self, fileI):

        pickle.dump(self, open(fileI, 'wb'))
#        pickle.dump(self.model, open(file+CLASS_SUFIX, 'wb'))

    @staticmethod
    def getModelFromDisk(fileModel):
        '''
           parameter 'fileModel' previously created and saved to disk, o 
        '''
        return pickle.load(open(fileModel, 'rb'))



################################################################
################ Functions Plotting Evaluation Data ################
def norm(x): 
    if x < 0:
        return 0.0
    if x > 1:
        return 1.0
    return round(x,5)


def count_interval(y_pred_norm, Ytest, clase, lim_inf, lim_sup):
    total = 0
    for i in range(0,len(y_pred_norm)):
        estimation = y_pred_norm[i]
        label = Ytest[i]
        if (label == clase and lim_inf <= estimation and estimation < lim_sup):
            total +=1 
    
    return total

def count(y_pred_norm, Ytest, clase, interval):
    count_dict = {}
    current = 0.0
    while current <1.0:
        next = round(current + interval,2)
        if current + interval < 1.0:
            x = count_interval(y_pred_norm, Ytest, clase, current, next)
            clave = str(current) + "-" + str(next)
            count_dict[clave] = x
            current =  next
        else:
            x = count_interval(y_pred_norm, Ytest, clase, current, 1.1)
            clave = str(current) + "-" + str(1.0)
            count_dict[clave] = x
            current =  next
    return count_dict



################ Functions Fbeta metric ################
def get_local_metric(Ytest, y_pred_norm, cut_point):
    y_pred_class = []
    for i in range(0,len(y_pred_norm)):
        estimation = y_pred_norm[i]
        if estimation < cut_point:
	        y_pred_class.append(0)
        else:
            y_pred_class.append(1)

#    cm = confusion_matrix(Ytest, y_pred_class)
#    print(cm)
    
    fbeta = fbeta_score(Ytest, y_pred_class, beta=1.0)
    return fbeta



def get_global_metric(Ytest, y_pred_norm, increment):
    metrics = {}
    cut_point = 0.0
    while cut_point <= 1.0:
        metrics[cut_point] = round(get_local_metric(Ytest, y_pred_norm, cut_point), 3)
        cut_point = round(cut_point + increment,2)
    
    best_cut_point = max(metrics, key=metrics.get)
    best_metric_value = metrics[best_cut_point]

    return best_cut_point, best_metric_value 


################# Functions Alvaro'metric ################
def get_min_estimation_class(y_pred_norm, Ytest, clase):
    min_value = 1.0
    for i in range(0,len(y_pred_norm)):
        estimation = y_pred_norm[i]
        label = Ytest[i]
        if (label == clase and estimation < min_value):
            min_value = estimation
    return min_value



def get_count_greater_estimation_class(y_pred_norm, Ytest, clase, estimation_point):
    count = 0
    for i in range(0,len(y_pred_norm)):
        estimation = y_pred_norm[i]
        label = Ytest[i]
        if (label == clase and  estimation_point < estimation): # a la derecha
            count +=1
    return count

def get_count_leq_estimation_class(y_pred_norm, Ytest, clase, estimation_point):
    count = 0
    for i in range(0,len(y_pred_norm)):
        estimation = y_pred_norm[i]
        label = Ytest[i]
        if (label == clase and estimation_point >= estimation): # a la izquierda
            count +=1
    return count



def main():

    strUsage= "Mode 1: python learn.py <MODELTYPE> <FILETRAIN> <FILESAVE> -> train a <MODELTYPE> model from <FILENAME> and  save to <FILESAVE>\n"
    strUsage +="[MODELTYPE]-> train or load a model, with MODELTYPE= LRCV or  LR or RF \n Where LR = Logistic Reg\n LRCV = Log. Reg with CrossValidation\n RF = Random Fores\n Default value MODELTYPE = LRCV\nSVM = Support Vector Machine (SVC)\nSVMCV = Support Vector Machine with CV grid search\nNBB=Naive Bayes classifier with Bernoulli estimator\nNBG=Naive Bayes classifier with Gaussian estimator\n DT  is decision Tree "
    strUsage +="Mode 2: python learn.py  <FILEMODEL> <FILETEST> -> returns the probabilities in SDOUT one 'line prob class 0', 'proba class 1'\n"
 
    if len(sys.argv) == 1:
        print (strUsage)
        tl = randBinList(94)
#        print tl
#        files = open("testfile.txt","w")  
#        files.write((",".join(''.join(str    (e) for e in tl)))+"\n") 
        
    elif len(sys.argv) == 3:
        fileLoadIn= sys.argv[1]
        fileTest=sys.argv[2]
        #fileYTest=sys.argv[3]

        #lernModel=LearnRules(fileModel=fileLoadIn)
        lernModel=LearnRules.getModelFromDisk(fileLoadIn)
        test = pd.read_csv(fileTest, header=None)
        Xtest= test.iloc[:,0:-1]
        Ytest= test.iloc[:, -1]
        
        y_pred=None
        if lernModel.modelType.endswith('RG'):
            #y_pred= lernModel.returnClasesList(Xtest)
            y_pred= lernModel.model.predict(Xtest)
        else:
            #y_pred= lernModel.model.predict(Xtest)
            y_pred= lernModel.returnProbClasesList(Xtest)
          

        ################# Regression #################
#        y_pred_norm = list(map(norm, y_pred)) 
#        interval=0.01	
#        
#        #Data Plot
#        print("Interval", "Class 0", "Class 1")
#        c0 = count(y_pred_norm, Ytest, 0, interval)
#        c1 = count(y_pred_norm, Ytest, 1, interval)
#        for k in c0.keys():
#            print(k, c0[k], c1[k])

#        #Metrics
#        x,value = get_global_metric(Ytest, y_pred_norm, interval)
#        print("Fbeta =", x, value)
#        estimation_point = get_min_estimation_class(y_pred_norm, Ytest,1)
#        p1 = get_count_leq_estimation_class(y_pred_norm, Ytest, 0, estimation_point)
#        p2 = get_count_greater_estimation_class(y_pred_norm, Ytest, 0, estimation_point)
#        print("Torralba =", p1/(p1+p2))



        ################# Classification #################
        y_pred_prob_class1 = []
        for v in y_pred:
            y_pred_prob_class1.append(v[1])

        y_pred_norm = list(map(norm, y_pred_prob_class1)) 
        interval=0.01
        interval_plot=0.05
        
        print("Interval	", "Class 0	", "Class 1")
        c0 = count(y_pred_norm, Ytest, 0, interval_plot)
        c1 = count(y_pred_norm, Ytest, 1, interval_plot)
        for k in c0.keys():
            print(k, "	",c0[k],"	", c1[k])

#       #Metrics
        x,value = get_global_metric(Ytest, y_pred_norm, interval)
        print("Fbeta =", x, value)
        estimation_point = get_min_estimation_class(y_pred_norm, Ytest,1)
        p1 = get_count_leq_estimation_class(y_pred_norm, Ytest, 0, estimation_point)
        p2 = get_count_greater_estimation_class(y_pred_norm, Ytest, 0, estimation_point)
        print("Torralba =", p1/(p1+p2))


    
    elif len(sys.argv) <=5 :
        fileIn= sys.argv[2]
        modType = sys.argv[1]      
        
        if len(sys.argv)==4:
            lernModel=LearnRules(fileTraining=fileIn, modelType=modType, njobs=4)
            lernModel.saveToDisk(sys.argv[3])
        elif(len(sys.argv)==5):
            balanced = sys.argv[4]
            bal=True if (balanced=='balanced')else False
            print (bal)
            lernModel=LearnRules(fileTraining=fileIn, modelType=modType, njobs=4,isBalanced=bal)
            lernModel.saveToDisk(sys.argv[3])
    
    else:
        print (strUsage)
        SystemExit("Bad Parameters\n"+strUsage)
            


        
main()


#print  model_cv1
#print classification_report(list(y_test), y_predClass_cv1, average='micro')

#
