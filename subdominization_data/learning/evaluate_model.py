#! /usr/bin/env python

from __future__ import print_function

import os
import io
import numpy as np

import shutil
import bz2
import string
import ast

import pickle

import csv
import sys
from sys import version_info

import helpers


is_python_3 = version_info[0] > 2 # test python version



if __name__ == "__main__":
    import argparse
    
    argparser = argparse.ArgumentParser()   
    argparser.add_argument("--runs-folder", required=True, help="folder containing the data to evaluate")
    argparser.add_argument("--model-folder", required=True, help="folder containing the models used for evaluation")
    argparser.add_argument("--models", required=True, help="models to be evaluated")
    argparser.add_argument("--output-folder", required=True, help="where to write evaluation data")    
    argparser.add_argument("--latex-output-file", help="file to write latex plots")
    argparser.add_argument("--latex-plots-folder-prefix", help="path used to points to latex plot data")

    options = argparser.parse_args()

    if (not os.path.exists(options.runs_folder)):
        sys.exit("path does not exist: " + options.runs_folder)
    if (not os.path.exists(options.model_folder)):
        sys.exit("file does not exist: " + options.model_folder)

    loaded_models = {}
    models_to_evaluate = ast.literal_eval(options.models)

    for trained_model in models_to_evaluate:
        loaded_models[trained_model] = {}
        folder = os.path.join(options.model_folder, "trained_model_" + trained_model + "/")
        for f in os.listdir(folder):
            if (f.endswith(".model")):
                with open(os.path.join(folder, f), "rb") as modelFile:
                    loaded_models[trained_model][f[:-6]] = pickle.load(modelFile)
        
        if (len(loaded_models[trained_model]) == 0):
            sys.exit("no trained models in " + folder)

    if (len(loaded_models) == 0):
        sys.exit("no trained models in " + options.model_folder)

    if (os.path.isdir(options.output_folder)):
        if (is_python_3):
            response = input("overwrite existing folder? y/N (" + options.output_folder + ") ")
        else:
            response = raw_input("overwrite existing folder? y/N (" + options.output_folder + ") ")
        if (not response in ["no", "No", "NO", "n", "N"]):
            shutil.rmtree(options.output_folder)
        else:
            sys.exit("folder already exists:", options.output_folder)
    
    os.makedirs(options.output_folder)
    
    schemas_per_model = {}
    
    for trained_model in models_to_evaluate:
        
        print(trained_model)
    
        schemas_per_model[trained_model] = set()
        
        for setting in ["training", "testing"]:
            
            all_zero_estimates = []
            all_one_estimates = []
            
            print("\t" + setting)
            
            runs_folder = os.path.join(options.runs_folder, setting)
        
            for f in os.listdir(runs_folder):
                if (f.endswith(".csv")):
                    eval_data_file = open(os.path.join(runs_folder, f))
                    schema = f[:-4]                
                elif(f.endswith(".csv.bz")):
                    eval_data_file = bz2.BZ2File(os.path.join(runs_folder, f), "r")
                    schema = f[:-7]
                else:
                    continue
                
                if (not schema in loaded_models[trained_model]):
                    print("skipping {schema} for {trained_model}".format(**locals()))
                    continue
                
                eval_data = csv.reader(eval_data_file, delimiter=",")
                schemas_per_model[trained_model].add(schema)
                
                error = 0.0
                n = 0
                
                zero_estimates = []
                one_estimates = []
                    
                for s in eval_data:
                    sample = list(map(int, s))
                    if (loaded_models[trained_model][schema].is_classifier):
                        estimate = loaded_models[trained_model][schema].model.predict_proba([sample[:-1]])[0][1]                
                    else:
                        estimate = loaded_models[trained_model][schema].model.predict([sample[:-1]])[0]
                            
                    error += pow(estimate - sample[-1], 2)
                    n += 1
                    if (sample[-1] == 0):
                        zero_estimates.append(estimate)
                        all_zero_estimates.append(estimate)
                    else:
                        assert(sample[-1] == 1)
                        one_estimates.append(estimate)
                        all_one_estimates.append(estimate)
                
                print("\t\t" + "MSE of ", schema, ": ", error / n)
                
                helpers.write_samples_to_file(zero_estimates, os.path.join(options.output_folder, trained_model, setting, schema + "-class0.txt"))
                helpers.write_samples_to_file(one_estimates, os.path.join(options.output_folder, trained_model, setting, schema + "-class1.txt"))
                        
            helpers.write_samples_to_file(all_zero_estimates, os.path.join(options.output_folder, trained_model, setting, "all-class0.txt"))
            helpers.write_samples_to_file(all_one_estimates, os.path.join(options.output_folder, trained_model, setting, "all-class1.txt"))
    
    if (options.latex_output_file):
        helpers.write_latex_file(schemas_per_model, models_to_evaluate, options.latex_plots_folder_prefix, options.latex_output_file)
                
                
                
                
                
                
                


