#!/bin/bash

#Dado un directorio con los actions models (*.model) de un dominio y el directorio con la matriz binaria de evaluacion para cada action model,
#devuelve por pantalla las metricas de evaluacion para cada action model

#LEARNING_FILE="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/learning/Mlearn.py"
#LEARNING_FILE="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/learning/Mplot.py"
#LEARNING_FILE="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/learning/MRules.py"
LEARNING_FILE="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/learning/Mplot_to_number.py"


REPO="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data"
DOMAIN="blocksworld"


ACTION_MODEL_FOLDER="action-model/LOGR/eval/train_n=2"
EVAL_DATA_FILE="training-data/eval/train_n=2" 


for model in $( ls $REPO/$DOMAIN/$ACTION_MODEL_FOLDER); do
	if [ $model != "relevant_rules" ]; then
		m="${model%.*}" #delete extension
		echo "Evaluating model " $model
		$LEARNING_FILE $REPO/$DOMAIN/$ACTION_MODEL_FOLDER/$model $REPO/$DOMAIN/$EVAL_DATA_FILE/$m.csv
	fi

done






