#! /usr/bin/env python

import os
#import shutil
#import subprocess

class Usefull_Rule:
	mass = 0.0
	str_rule = ""

	def __init__(self,mass,str_rule):
		self.mass = mass
		self.str_rule = str_rule

	def get_mass(self):
		return self.mass

	def get_str_rule(self):
		return self.str_rule

	def pretty_print(self):
		print(self.get_mass(), self.get_str_rule())

	def get_schema_name(self):
		return self.str_rule.split()[0]		

	def is_equal(self, rule2):
		return self.get_str_rule() == rule2.get_str_rule()		

	def write_rule(self,f): # f file object
		f.write(self.get_str_rule() + "\n")


class Usefullnes:
	rule_dict = {} #usefull_rule dictionary schema -> [rules]


	def __init__(self, usefulness_file):
		self.rule_dict = {}
		f = open(usefulness_file, "r")
		lines = f.readlines()
		for line in lines:
			mass = float(line.split("\t")[0].strip())
			str_rule = line.split("\t")[1].strip()
			rule = Usefull_Rule(mass, str_rule)
			schema = rule.get_schema_name()
			if schema in self.rule_dict:
				self.rule_dict[schema].append(rule)
			else:
				self.rule_dict[schema] = [rule]
		f.close()



	def get_rank(self, schema, rule2):
		rank=0
		for rule in self.rule_dict[schema]:
			if rule.get_str_rule() == rule2.get_str_rule():
				return rank
			else:
				rank+=1 

	def pretty_print(self, schema):
		if schema in self.rule_dict:
			for rule in self.rule_dict[schema]:
				rule.pretty_print()
			print(len(self.rule_dict[schema]))


	def get_filter_by_mass(self, schema, mass):
		filter_rules = []
		for rule in self.rule_dict[schema]:
#			if rule.get_mass() >= mass and rule.get_mass() > self.get_max_mass(schema) /10:
			if rule.get_mass() >= mass:
				filter_rules.append(rule)
		return filter_rules

	def get_filter_by_rank(self, schema, rank):
		filter_rules = []
		i=1
		for rule in self.rule_dict[schema]:
			if i<= rank:
				filter_rules.append(rule)
				i+=1
			else:
				break
		return filter_rules



	def get_number_rules(self, schema):
		return len(self.rule_dict[schema])		
	def get_number_rules_by_mass(self, schema, mass):
		return len(self.get_filter_by_mass(schema,mass))		
	def get_number_rules_by_rank(self, schema, rank):
		return len(self.get_filter_by_rank(schema,rank))		


	def get_rules_by_mass(self, schema, mass):
		for rule in self.get_filter_by_mass(schema,mass):
			rule.pretty_print()
	def get_rules_by_rank(self, schema, rank):
		for rule in self.get_filter_by_rank(schema,rank):
			rule.pretty_print()
		

	def write_rules_by_rank(self, schema, rank, path_file):
		f = open(path_file, "a")
		for rule in self.get_filter_by_rank(schema,rank):
			rule.write_rule(f)
		f.close()


	def to_relevant_rules(self, all_schemas):
		print("Filtering rules by rank ...")
		for schema in all_schemas:
			for rank in [1000]:
				path_file = "/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/satellite/training-data/optimal/step3-4/useful_rules_DT_rank=" + str(rank) 
				u1.write_rules_by_rank(schema,rank, path_file)
		print("Done.")




	def get_max_mass(self,schema):
		m = 0.0
		for rule in self.rule_dict[schema]:
			m = max(m,rule.get_mass())
		return m


	def get_overlap(self, usefullnes2, schema, mass1, mass2):
		usefull_rules1 = self.get_filter_by_mass(schema,mass1)
		usefull_rules2 = usefullnes2.get_filter_by_mass(schema,mass2)

		total = 0
		for rule1 in usefull_rules1:
			for rule2 in usefull_rules2:
				if rule1.is_equal(rule2):
					total+=1
					print(self.get_rank(schema,rule1),rule1.get_mass(), usefullnes2.get_rank(schema,rule2), rule2.get_mass(), rule1.get_str_rule())				
		
		print("Total overlap:",total)
			
	

if __name__ == "__main__":

	# configurable parameters
	bench_folder = "/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data"
	domain_folder = "satellite"

	rule_usefulness_file1 = os.path.join(bench_folder,domain_folder, "training-data/optimal/step3-4/rule_usefulness_dt")
#	rule_usefulness_file2 = os.path.join(bench_folder,domain_folder, "training-data/eval/train_n=2/step3-4/rule_usefulness_rf")

	u1 = Usefullnes(rule_usefulness_file1)
#	u2 = Usefullnes(rule_usefulness_file2)


	all_schemas = ["calibrate", "switch_on", "switch_off", "take_image", "turn_to"]
	for schema in all_schemas:
		print(schema, u1.get_number_rules(schema))
	
	u1.to_relevant_rules(all_schemas)


#	schema = "calibrate"
#	print("mass=", mass)
#	print("filter:", len(u1.get_filter(schema, mass)), "max_mass:",u1.get_max_mass(schema))
#	print("filter:", len(u2.get_filter(schema, mass)), "max_mass:", u2.get_max_mass(schema))
#	u1.get_overlap(u2, schema, mass, mass)
	

