#!/bin/bash

#Dado una directorio con los runs y un archivo de reglas relacionales, evalua las relgas para devolver la matriz binaria de datos correspondiente

SCRIPTS="/home/fbustos/planning/subdominization/fd-subdominization/src/subdominization-training"

REPO="/home/fbustos/planning/subdominization/subdominization_data"
DOMAIN="satellite"


RUN="runs/eval/test"												#data examples 
RULES="training-data/eval/train/step6/KRN_RG/relevant_rules"		#rules to evaluate (projection)
MATRIX_STORE="training-data/eval/test/train"						#where to store data matrix							

OPTIMAL_OPERATORS="good_operators" 

# 5- generate binary matrix from run and rules
echo "Step5: building binary matrix"
mkdir -p $REPO/$DOMAIN/$MATRIX_STORE/
python2 $SCRIPTS/gen-subdom-training.py $REPO/$DOMAIN/$RUN/ $REPO/$DOMAIN/$RULES $REPO/$DOMAIN/$MATRIX_STORE/ --op-file $OPTIMAL_OPERATORS






