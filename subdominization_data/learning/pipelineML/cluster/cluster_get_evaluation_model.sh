#!/bin/bash

#Dado un directorio con los actions models (*.model) de un dominio y el directorio con la matriz binaria de evaluacion para cada action model,
#devuelve por pantalla las metricas de evaluacion para cada action model

LEARNING_FILE="/home/fbustos/planning/subdominization/subdominization_data/learning/Mlearn.py"

REPO="/home/fbustos/planning/subdominization/subdominization_data"
DOMAIN="satellite"


ACTION_MODEL_FOLDER="training-data/eval/train/step6/KRN_RG"
EVAL_DATA_FILE="training-data/eval/test/train" 


for model in $( ls $REPO/$DOMAIN/$ACTION_MODEL_FOLDER); do
	if [ $model != "relevant_rules" ]; then
		m="${model%.*}" #delete extension
		echo "Evaluating model " $model
		$LEARNING_FILE $REPO/$DOMAIN/$ACTION_MODEL_FOLDER/$model $REPO/$DOMAIN/$EVAL_DATA_FILE/$m.csv
	fi
	#break	

done






