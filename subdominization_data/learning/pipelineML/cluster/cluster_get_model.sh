#!/bin/bash

#Devuelve los action models de un dominio, a partir de un un archivo de reglas o generando las reglas (step1)  

SCRIPTS_="/home/fbustos/planning/subdominization/subdominization_data/learning"					 #step 4,6

REPO="/home/fbustos/planning/subdominization/subdominization_data"
DOMAIN="satellite"

CSV_FOLDER="training-data/xxx/csv-useful_rules_DT_rank=100"
MODEL_FOLDER="training-data/xxx/csv-useful_rules_DT_rank=100"
MODEL_TYPE="KRN_RG"


## Step6: generating action models 
echo "Step6: building action models"
python3 $SCRIPTS_/train_model_for_domain.py --training-set-folder $REPO/$DOMAIN/$CSV_FOLDER --model-folder $REPO/$DOMAIN/$MODEL_FOLDER/$MODEL_TYPE --model-type $MODEL_TYPE --mean-over-duplicates





