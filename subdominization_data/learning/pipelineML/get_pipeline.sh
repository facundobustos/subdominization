#!/bin/bash

#Devuelve los action models de un dominio, a partir de un un archivo de reglas o generando las reglas (step1)  

SCRIPTS="/home/facundo/Documents/MySubdominization/Posdoc/fd-subdominization/src/subdominization-training"
SCRIPTS_="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data/learning"

REPO="/home/facundo/Documents/MySubdominization/Posdoc/subdominization_data"
DOMAIN="satellite"
RUN="eval/train_n=3"

MODEL_TYPE="KRN_RG"

SELECTOR_TYPE="DT"
SELECTOR_THRESHOLD="0.0"

OPTIMAL_OPERATORS="good_operators" 


# Step1: generates rules by force brute (set the parameter below, please)
#RULES_SIZE=3
#RULES_NUM=100  
#RULES_FILE="rules_"$RULES_SIZE"_$RULES_NUM"
#echo "Step1: generatig rules by force brute"
#mkdir -p $REPO/$DOMAIN/training-data/$RUN/step1
#python2 $SCRIPTS/gen-subdom-rules.py $REPO/$DOMAIN/domain.pddl --store_rules $REPO/$DOMAIN/training-data/$RUN/step1/$RULES_FILE --rule_size $RULES_SIZE --num_rules $RULES_NUM


# Step1: brute rules already generated in "rules" directory 
RULES_FILE="relevant_num10k" 
echo "Step1: force brute rules has been already generated in $DOMAIN/rules/$RULES_FILE"  
mkdir -p $REPO/$DOMAIN/training-data/$RUN/step1
cp $REPO/$DOMAIN/rules/$RULES_FILE $REPO/$DOMAIN/training-data/$RUN/step1

# Step2: filter those rules that evaluates all to 0,1 and "subrules"
echo "Step2: filtering rules"
mkdir -p $REPO/$DOMAIN/training-data/$RUN/step2
python2 $SCRIPTS/gen-relevant-rules.py $REPO/$DOMAIN/runs/$RUN/ $REPO/$DOMAIN/training-data/$RUN/step1/$RULES_FILE $REPO/$DOMAIN/training-data/$RUN/step2/$RULES_FILE"_relevant"

# Step3: generating binary training matrix 
echo "Step3: building binary training matrix"
python2 $SCRIPTS/gen-subdom-training.py $REPO/$DOMAIN/runs/$RUN/ $REPO/$DOMAIN/training-data/$RUN/step2/$RULES_FILE"_relevant" $REPO/$DOMAIN/training-data/$RUN/step3-4 --op-file $OPTIMAL_OPERATORS

# Step4: selecting features with selector_type
echo "Step4: selecting features"
python3 $SCRIPTS_/select_features.py --training-folder $REPO/$DOMAIN/training-data/$RUN/step3-4 --selector-type $SELECTOR_TYPE --threshold $SELECTOR_THRESHOLD

# Step5: regenerating binary training matrix 
echo "Step5: rebuilding binary training matrix"
python2 $SCRIPTS/gen-subdom-training.py $REPO/$DOMAIN/runs/$RUN/ $REPO/$DOMAIN/training-data/$RUN/step3-4/useful_rules_$SELECTOR_TYPE $REPO/$DOMAIN/training-data/$RUN/step5 --op-file $OPTIMAL_OPERATORS

## Step6: generating action models 
echo "Step6: building action models"
python3 $SCRIPTS_/train_model_for_domain.py --training-set-folder $REPO/$DOMAIN/training-data/$RUN/step5 --model-folder $REPO/$DOMAIN/training-data/$RUN/step6/$MODEL_TYPE --model-type $MODEL_TYPE --mean-over-duplicates





