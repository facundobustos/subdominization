#! /usr/bin/env python

from __future__ import print_function

import os
import io
import numpy as np
from pddl_parser import pddl_file
from pddl_parser import parsing_functions

from rule_evaluator_aleph import RuleEvaluatorAleph

import shutil
import bz2
import string

import pickle

import helpers

import csv

from sys import version_info
from numpy.distutils.log import good


is_python_3 = version_info[0] > 2 # test python version

try:
    # Python 3.x
    from builtins import open as file_open
except ImportError:
    # Python 2.x
    from codecs import open as file_open








def write_latex_file(schemas, data_path_prefix, file):
    if (os.path.isfile(file)): # query the user if overwrite file
        if (is_python_3):
            response = input("overwrite existing file? (" + file + ") y/N ")
        else:                
            response = raw_input("overwrite existing file? (" + file + ") y/N ")
        if (response in ["no", "No", "NO", "n", "N"]):
            print("file already exists", file)
            sys.exit(1)
        else:
            os.remove(file)
            
    with open(file, "w") as outfile:
        outfile.write("""\\documentclass{article}\n\n"""
                   """\\usepackage{filecontents}\n"""
                   """\\usepackage{pgfplots, pgfplotstable}\n"""
                   """\\usepgfplotslibrary{statistics,external}\n"""
                   """\n"""
                   """\\tikzexternalize[prefix=TMP/]\n"""
                   """\n"""
#                    """\\usepackage{listings}\n""" # didnt work
#                    """\\newcommand{\\codelst}{\\begingroup\n"""
#                    """  \\catcode`_=12 \\docodelst}\n"""
#                    """\\newcommand{\\docodelst}[1]{%\n"""
#                    """  \\lstinputlisting[caption=\\texttt{#1}]{#1}%\n"""
#                    """  \\endgroup\n"""
#                    """}\n\n"""
                   """\\input{plot_macro}\n"""
                   """\n"""
                   """\\begin{document}\n\n""")
        outfile.write("\\section{Aleph}\n\n")
#         outfile.write("training set left, validation set right\n\n")
        for schema in schemas:
            stripped_schema = schema.replace("_", "-")
            outfile.write("\histplot{{{data_path_prefix}}}{{{schema}}}{{{stripped_schema}}}\n\n".format(**locals()))
        outfile.write("\histplot{{{data_path_prefix}}}{{all}}{{all}}\n\n".format(**locals()))
                
        outfile.write("\\end{document}\n")










if __name__ == "__main__":
    import argparse
    
    argparser = argparse.ArgumentParser()   
    argparser.add_argument("--runs-folder", required=True, help="folder containing the data to evaluate")
    argparser.add_argument("--aleph-model", required=True, help="file containing the trained model used for evaluation")
    argparser.add_argument("--output-folder", required=True, help="where to write evaluation data")    
    argparser.add_argument("--latex-output-file", help="file to write latex plots")
    argparser.add_argument("--latex-plots-folder-prefix", help="path used to points to latex plot data")

    options = argparser.parse_args()

    if (not os.path.exists(options.runs_folder)):
        sys.exit("path does not exist: " + options.runs_folder)
    if (not os.path.exists(options.aleph_model)):
        sys.exit("file does not exist: " + options.aleph_model)

    if (os.path.isdir(options.output_folder)):
        if (is_python_3):
            response = input("overwrite existing folder? y/N (" + options.output_folder + ") ")
        else:
            response = raw_input("overwrite existing folder? y/N (" + options.output_folder + ") ")
        if (not response in ["no", "No", "NO", "n", "N"]):
            shutil.rmtree(options.output_folder)
        else:
            sys.exit("folder already exists:", options.output_folder)
    
    os.makedirs(options.output_folder)
    
    
    
    schemas = set()   
    
    all_zero_estimates = []
    all_one_estimates = []
    
    zero_estimates = {}
    one_estimates = {}
    
    error = {}
    n = {}
    
    for run in os.listdir(options.runs_folder):
        
        run_folder = os.path.join(options.runs_folder, run, "")

        task = pddl_file.open(os.path.join(run_folder, "domain.pddl"), os.path.join(run_folder, "problem.pddl"))
        
        if (not os.path.isfile(os.path.join(run_folder, "good_operators"))):
            print("no good operators for", run)
            continue
        
        with open(os.path.join(run_folder, "good_operators"), "r") as good_ops_file:
            good_ops = set(map(lambda x : tuple(x.replace("\n", "").replace(")", "").replace("(", "").split(" ")), good_ops_file.readlines()))
            
        with bz2.BZ2File(os.path.join(run_folder, "all_operators.bz2"), "r") as all_ops_file:
            all_ops = set(map(lambda x : tuple(x.decode("utf-8").replace("\n", "").replace("(", " ").replace(")", "").replace(",", "").split(" ")), all_ops_file.readlines()))
        
        with open(options.aleph_model, "r") as aleph_rules:
            model = RuleEvaluatorAleph(aleph_rules.readlines(), task)
            
        
        for action in all_ops:            
            schema = action[0]
            
            schemas.add(schema)
            
            estimate = model.get_estimate_string_tuple(action)
            
            if (not schema in error):
                error[schema] = 0.0
                n[schema] = 0
                zero_estimates[schema] = []
                one_estimates[schema] = []
                
            if (action in good_ops):
                error[schema] += pow(1 - estimate, 2)
                one_estimates[schema].append(estimate)
                all_one_estimates.append(estimate)
            else:
                error[schema] += pow(0 - estimate, 2)
                zero_estimates[schema].append(estimate)
                all_zero_estimates.append(estimate)
            
            n[schema] += 1

    
    for schema in schemas:
        helpers.write_samples_to_file(zero_estimates[schema], os.path.join(options.output_folder, schema + "-class0.txt"))
        helpers.write_samples_to_file(one_estimates[schema], os.path.join(options.output_folder, schema + "-class1.txt"))
        print("MSE of ", schema, ": ", error[schema] / n[schema])
                        
    helpers.write_samples_to_file(all_zero_estimates, os.path.join(options.output_folder, "all-class0.txt"))
    helpers.write_samples_to_file(all_one_estimates, os.path.join(options.output_folder, "all-class1.txt"))
    
    if (options.latex_output_file):
        write_latex_file(schemas, options.latex_plots_folder_prefix, options.latex_output_file)
        
        
        
        
        