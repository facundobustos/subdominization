#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys

#from driver.main import main

class GroundedAction:
	name = None
	objects = []

	def __init__(self, name, objects):
		self.name = name
		self.objects = objects

	def get_name(self):
		return self.name

	def get_objects(self):
		return self.objects

	def get_object(self, index):
		return self.objects[index]

	def get_index(self, obj):
		if (obj in self.get_objects()):
			return self.objects.index(obj)
		else:
			return -1

	def get_arity(self):
		return len(self.objects)

	def to_string(self):
		if self.get_arity() == 0:
			return self.name + "()"
		else:

			s = self.name + "("
			for obj in self.objects[0:self.get_arity()-1]:
				s+= obj + ","
			s+=self.get_object(self.get_arity()-1)
			s+=")"
			return s

	def equal(self,b): #are the same grounded action as string
		return self.to_string() == b.to_string()

	def print_(self):
		print(self.to_string())

	def total_covered(self, b):
		#interfaz(self) subset interfaz(b)
		for x in self.get_objects():
			if not(x in b.get_objects()):
				return False
			else:
				continue
		return True

	def partial_covered(self, b):
		#interfaz(self) intersection interfaz(b) != {}
		for x in self.get_objects():
			if (x in b.get_objects()):
				return True
			else:
				continue
		return False



if __name__ == "__main__":
	a = GroundedAction("move",["A","B","C"])
	a.print_()

	b = GroundedAction("load",["E", "B"])
	b.print_()

	c = GroundedAction("load", ["E", "B"])

	print(b.equal(c))
	print("Done.")



