#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys

from grounded_action import GroundedAction
from action_schema import ActionSchema


SEPARATOR_SYMBOL = " "


class RuleInstance:
	task = None  				#the task name in which the instance appears
	left = None 				#the grounded action in left side of the rule instance
	right = None 				#the grounded action in right side of the rule instance
	str_rule_instance = None 	#the string characterization of a rule_instance

	def __init__(self, task, a1, a2):
		self.task = task
		self.left = a1
		self.right = a2
		self.str_rule_instance = task + ":" + a1.to_string() + a2.to_string()

	def to_string(self):
		return self.task + ": " + self.left.to_string() + SEPARATOR_SYMBOL + self.right.to_string()

	def print_(self):
		print(self.to_string())

	def get_task(self):
		return self.task

	def get_left(self):
		return self.left
	
	def get_right(self):
		return self.right

	def get_str_rule_instance(self):
		return self.str_rule_instance


	def is_rule(self):
		return self.get_right().partial_covered(self.get_left()) # a(O1),b(O2) form a general rule a(X) => b(Y)


	def generalize(self):
		#return action schemas a,b s.t a => b is a generalization of this rule_intance

		# build the left side of the rule
		a = ActionSchema(self.get_left().get_name())
		for obj in self.get_left().get_objects():
			idx = self.get_left().get_objects().index(obj)  # an object may be more one times in same interface
			a.add_var("X" + str(idx))

		# build the right side of the rule
		b = ActionSchema(self.get_right().get_name())
		free_var_index = a.get_arity()
		for obj in self.get_right().get_objects():
			idx = self.left.get_index(obj)
			if idx == -1: #is a free variable in right side of the rule
				b.add_var("X" + str(free_var_index))
				free_var_index+= 1
			else: # is a bounded variable
				b.add_var("X" + str(idx))

		return a,b





if __name__ == "__main__":
	a1 = GroundedAction("drive",["truck1","depot0","distributor0"])
	a2 = GroundedAction("lift",["hoist1","crate0","pallet1","distributor0"])

	rule_instance = RuleInstance("p01-1-1-2-2-2-4",a1,a2)

	a, b = rule_instance.generalize()
	a.print_()
	b.print_()

	print("Done.")

