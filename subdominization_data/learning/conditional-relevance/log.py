#! /usr/bin/env python

import os
import sys

#from driver.main import main


class Log:
	logfile_path = None

	def __init__(self, logfile_path):
		self.logfile_path = logfile_path

	def get_logfile_path(self):
		return self.logfile_path


	def print_line(self, line):
		f = open(self.get_logfile_path(), "a+")
		f.write("%s\n" %(line))
		f.close()



if __name__ == "__main__":

	logfile_path= "/home/facundo/Documents/MySubdominization/fd_subdominization/src/subdominization-training/subdominization_data/blocksworld/rules/conditional_rules/log"
	log = Log(logfile_path)
	log.print_line("hola")
	log.print_line("chau")

	print("Done.")

