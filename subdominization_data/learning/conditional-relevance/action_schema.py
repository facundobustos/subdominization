#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys



class ActionSchema:
	name = None 		#the string name of the action schema
	variables = None	#list of string variables of the action schema

	def __init__(self, name):
		self.name = name
		self.variables = []

	def get_name(self):
		return self.name

	def get_var(self, index):
		return self.variables[index]

	def add_var(self, var):
		self.variables.append(var)

	def is_var(self,var):
		return var in self.variables

	def get_index(self, var):
		if (var in self.get_variables()):
			return self.variables.index(var)
		else:
			return -1

	def get_arity(self):
		return len(self.variables)


	def to_string(self):
		if self.get_arity() == 0:
			return self.name + "()"
		else:
			s = self.name + "("
			for var in self.variables[0:self.get_arity()-1]:
				s+= var + ","
			s+=self.get_var(self.get_arity()-1)
			s+=")"
			return s

	def print_(self):
		print(self.to_string())



if __name__ == "__main__":
	a = ActionSchema("move")
	a.add_var("x9")
	a.add_var("x1")
	a.print_()
	print("Done.")



