#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys

#from driver.main import main

from grounded_action import GroundedAction 
from pair import Pair 


class Pop:
	secuence = [] #secuence of pairs of grounded actions a < b

	def __init__(self):
		self.secuence = []

#	def popgen_to_pop(self, pop_file):
#		f = open(pop_file,"r")
#		lines =f.readlines()
#		for line in lines:
#			if not line.startswith(";"): #it's not a comment#
#				if "is ordered before" in line: #it's a restriction
#					if not("(init)" in line) and not("(goal)" in line): #it's not init or goal restriction
#						line = line.strip("\n")
#						line = line.lstrip("0123456789 ") #remove the number prefix
#						pair = line.split(" is ordered before ") 
#						if (pair[0] != pair[1]): #it's not a reflexive pair
#							pair[0] = pair[0].strip("()") #remove parenthesis
#							pair[1]= pair[1].strip("()") #remove parenthesis
#							
#							#getting a <= b
#							a_name = pair[0].split()[0]  									
#							a_objects = pair[0].split()[1:]	
#							a = GroundedAction(a_name,a_objects)

#							b_name = pair[1].split()[0]  									
#							b_objects = pair[1].split()[1:]	
#							b = GroundedAction(b_name,b_objects)
#		
#							pair = Pair(a,b)
#							self.add_pair(pair)
#		f.close()

	def popgen_to_pop(self, pop_file):
		f = open(pop_file,"r")
		lines =f.readlines()
		for line in lines:
			if not line.startswith(";"): #it's not a comment#
				if "supports" in line: #it's a restriction
					if not("(init)" in line) and not("(goal)" in line): #it's not init or goal restriction
						line = line.strip("\n")
						line = line.lstrip("0123456789 ") #remove the number prefix
						pair = line.split(" supports ") 

						a = pair[0]
						b = pair[1].split(" with")[0]

						if (a != b): #it's not a reflexive pair
							a = a.strip("()") #remove parenthesis
							b = b.strip("()") #remove parenthesis

							#build a <= b
							a_name = a.split()[0]  									
							a_objects = a.split()[1:]	
							a = GroundedAction(a_name,a_objects)

							b_name = b.split()[0]  									
							b_objects = b.split()[1:]	
							b = GroundedAction(b_name,b_objects)
		
							self.add_pair(Pair(a,b))
							#a.print_()
							#b.print_()
							#break

		f.close()


	def get_pair(self, index):
		return self.secuence[index]

	def add_pair(self, pair):
		self.secuence.append(pair)

	def get_len(self):
		return len(self.secuence)


	def print_(self):
		print("-------- POP ----------")
		for pair in self.secuence:
			pair.print_()			
		print("-------- END-POP -------------")


	def get_hard_rules(self, hard_rules_file_output):
		f = open(hard_rules_file_output,"a")
		#f.write("---------------------- POP hard-rules ----------------------\n")		
		for p in self.secuence: #for each pair a < b in secuence			
			a = p.pair[0] 
			b = p.pair[1]
			if b.total_covered(a): #found a hard-rule

				#write the left side of the hard rule					
				f.write("%s(" %a.get_name()) 
				for k in range(0,a.get_arity()-1):
					ob = a.get_object(k)
					idx = a.get_objects().index(ob) #because an object may be more one times in same interface
					f.write("X%i," %idx)
				#last variable of the antecedent to avoid to write ","
				ob = a.get_object(a.get_arity()-1)
				idx = a.get_objects().index(ob)
				f.write("X%i) => " %idx)


				#write the right side of the hard rule
				f.write("%s(" %b.get_name())
				for k in range(0,b.get_arity()-1):
					ob =  b.get_object(k)
					idx = a.get_objects().index(ob) #idx from interface(a) 
					f.write("X%i," %idx)
				#last variable of the consecuent to avoid to write ","
				ob =  b.get_object(b.get_arity()-1)
				idx = a.get_objects().index(ob)
				f.write("X%i)\n" %idx)
		f.write("--------------------------------------------------------------\n")				
		f.close()
				

	def action_frecuency(self, dic): #dic := <action_schema_name, frecuency> 
		for p in self.secuence: #a < b
			a = p.get_a() 
			b = p.get_b()
			if a.get_name() in dic.keys(): #check for action schema a
				dic[a.get_name()] += 1
			else:
				dic.update({a.get_name():1})
#			if b.get_name() in dic.keys(): #check for action schema b
#				dic[b.get_name()] += 1
#			else:
#				dic.update({b.get_name():1})



if __name__ == "__main__":

	pop = Pop()
	pop.popgen_to_pop("/home/facundo/Documents/MySubdominization/pop-gen/bench/pop.map")	
	pop.print_()
	dic = {}
	pop.action_frecuency(dic)
	for key,value in dic.items():
		print(key,value)
	#pop.get_hard_rules("hard_rules")
	print("Done.")

