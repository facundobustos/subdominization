#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys

from action_schema import ActionSchema
from grounded_action import GroundedAction
from rule_instance import RuleInstance



SEPARATOR_SYMBOL = " "
RULE_SYMBOL = " => "
POSITIVE_SYMBOL = "+ "
NEGATIVE_SYMBOL = "- "



class Rule:
	str_rule = None			#the string characterization of the rule
	left_operator = None	#action schema of the left side of the rule
	right_operator = None  	#action schema of the right side of the rule

	positive_instances = None 	#list of rule_instance
	negative_instances = None 	#list of rule_instance

	frequency_distinct = 0		#how many different plans rule appears



	def __init__(self, left_operator,right_operator):
		self.str_rule = left_operator.to_string() + RULE_SYMBOL + right_operator.to_string()
		self.left_operator = left_operator
		self.right_operator = right_operator
		self.positive_instances = []
		self.negative_instances = []
		self.frequency_distinct = 1


	def print_(self):
		#print rule string
		print(self.str_rule)

		#print positive and negative instances of the rule
		for rule_instance in self.positive_instances:
			print(POSITIVE_SYMBOL, rule_instance.to_string())
		for rule_instance in self.negative_instances:
			print(NEGATIVE_SYMBOL, rule_instance.to_string())




	def get_str_rule(self):
		return self.str_rule

	def get_left_operator(self):
		return self.left_operator

	def get_right_operator(self):
		return self.right_operator

	def equal(self, str_rule):
		return self.str_rule == str_rule

	def get_rule_frequency(self):
		return len(self.positive_instances)

	def get_rule_frequency_distinct(self):
		return self.frequency_distinct

	def inc_frequency_distinct(self):
		self.frequency_distinct += 1


	def add_positive_instance(self, rule_instance):
		self.positive_instances.append(rule_instance)

	def add_negative_instance(self, rule_instance):
		self.negative_instances.append(rule_instance)

	def has_positive_instances(self):
		return len(self.positive_instances) != 0

	def number_positive_instances(self):
		return len(self.positive_instances)

	def number_negative_instances(self):
		return len(self.negative_instances)

	def number_total_instances(self):
		return len(self.positive_instances) + len(self.negative_instances)

	def number_task(self):
		task_list = []
		total_pos = 0
		for instance in self.positive_instances:
			if instance.get_task() not in task_list:
				task_list.append(instance.get_task())
				total_pos += 1

		task_list = []
		total_neg = 0
		for instance in self.negative_instances:
			if instance.get_task() not in task_list:
				task_list.append(instance.get_task())
				total_neg += 1

		return total_pos, total_neg

	def rule_filter(self):
		return self.number_positive_instances() >= 1


	def is_positive(self, str_rule_instance):
		for rule_instance in self.positive_instances:
			if rule_instance.get_str_rule_instance() == str_rule_instance:
				return True
		return False



if __name__ == "__main__":
	a1 = GroundedAction("move", ["A", "B", "C"])
	a2 = GroundedAction("load", ["B", "D"])
	t = RuleInstance("p01-1-1-2-2-2-4", a1, a2)

	a,b = t.generalize()
	rule = Rule(a,b)
	rule.add_positive_instance(t)

	rule.print_()
	a,b = rule.number_task()
	print(a)
	print(b)

	print("Done.")

