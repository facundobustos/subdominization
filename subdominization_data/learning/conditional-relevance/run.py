#! /usr/bin/env python

import os
import shutil
import argparse
import subprocess
import sys

from plan import Plan
from rule import Rule
from log import Log


#constants for write_rules() method
SEPARATOR_SYMBOL = " "
POSITIVE_SYMBOL = "+ "
NEGATIVE_SYMBOL = "- "



class Run:
	plan_list = None			#list of plans corresponding to a run
	rules = None 				#list of rules corresponding to a run
	operator_frequency = None 	#dictionary with the number of times that the operator appears
	log = None

	def __init__(self, bench, domain, runs_folder, rules_folder):
		self.plan_list = []
		self.rules = []
		self.operator_frequency = {}

		rules_file = os.path.join(rules_folder, "all_rules")
		self.log = Log(rules_file + "_log")

		task_list = os.listdir(os.path.abspath(runs_folder))
		task_list.sort()

		for folder in task_list:
			task_folder = os.path.join(runs_folder, folder)
			if os.path.isdir(task_folder):
				sas_plan_file = os.path.join(task_folder, "sas_plan")
				all_operators_file = os.path.join(task_folder, "all_operators.bz2")
				if os.path.exists(sas_plan_file) and os.path.exists(all_operators_file):

					print(folder)
					self.log.print_line(folder)
					plan = Plan(folder)
					plan.sasplan_to_plan(sas_plan_file)
					plan.build_hash(all_operators_file)
					plan.search_rules()
					plan.search_negative_instances()
					plan.search_operators_frecuencies()

					self.plan_list.append(plan)
					#self.search_operators_frecuencies(plan.task)
					#self.search_rules(plan.task)
					#self.write_rules(rules_file)
					#break

		self.search_operators_frecuencies()
		self.search_rules()
		self.new_write_rules(rules_folder)


		self.log.print_line("Done.")



	def search_operators_frecuencies(self):
		for plan in self.plan_list:
			for k,v in plan.operator_frequency.items():
				if k in self.operator_frequency.keys():
					self.operator_frequency[k] += v
				else:
					self.operator_frequency.update({k:v})



	def search_rules(self):
		for plan in self.plan_list:
			for rule in plan.rules:
				if self.rule_already_exists(rule.get_str_rule()): #the rule exists in the run
					r = self.get_rule(rule.get_str_rule())
					r.inc_frequency_distinct()

					#add all positive instances
					for positive in rule.positive_instances:
						r.add_positive_instance(positive)

					#add all negative instances
					for negative in rule.negative_instances:
						r.add_negative_instance(negative)

				else: #the rule does not exists in the run
					r = Rule(rule.get_left_operator(), rule.get_right_operator())

					#add all positive instances
					for positive in rule.positive_instances:
						r.add_positive_instance(positive)

					#add all negative instances
					for negative in rule.negative_instances:
						r.add_negative_instance(negative)

					self.rules.append(r)






	def rule_already_exists(self, str_rule):
		for rule in self.rules:
			if rule.equal(str_rule): #the rule already exists in the run
				return True
			else:
				continue
		return False


	def get_rule(self, str_rule):
		for rule in self.rules:
			if rule.equal(str_rule):  # the rule was found
				return rule
			else:
				continue
		return None

	def get_number_plans(self,):
		return len(self.plan_list)

	def get_operator_frequency(self,operator):
		return self.operator_frequency[operator]

	def print_(self):
		for rule in self.rules:
			if rule.has_positive_instances():
				rule.print_()


	def write_rules(self, rules_file):
		f = open(rules_file, "w")
		for rule in self.rules:
			if rule.has_positive_instances():
				number_plans = self.get_number_plans()
				operator_frequency = self.get_operator_frequency(rule.get_left_operator().get_name())
				rule_frequency = rule.get_rule_frequency()
				rule_frequency_distinct = rule.get_rule_frequency_distinct()
				rate = rule_frequency / operator_frequency
				str_rule = rule.get_str_rule()

				#Here we write the head of the rule
				f.write("%i %i %i %i %.2f	%s\n" %(number_plans,operator_frequency,rule_frequency,rule_frequency_distinct,rate,str_rule))

				# Here we write the positive and negative instances of the rule
				for rule_instance in rule.positive_instances:
					f.write("%s %s\n" %(POSITIVE_SYMBOL, rule_instance.to_string()))
				for rule_instance in rule.negative_instances:
					f.write("%s %s\n" %(NEGATIVE_SYMBOL, rule_instance.to_string()))

		f.close()

	def new_write_rules(self, rules_folder):
		for rule in self.rules:
			if rule.rule_filter(): #filter
				rules_file = os.path.join(rules_folder, rule.get_str_rule())
				f = open(rules_file, "w")

				number_plans = self.get_number_plans()
				number_positives = rule.number_positive_instances()
				number_negatives = rule.number_negative_instances()
				number_instances = rule.number_total_instances()
				proportion = number_positives / number_instances
				number_task_pos, number_task_neg = rule.number_task()
				str_rule = rule.get_str_rule()

				# Here we write the head of the rule
				f.write("%i %i %i %i %.2f %i %i %s\n" % (number_plans, number_instances, number_positives, number_negatives, proportion, number_task_pos, number_task_neg, str_rule))

				# Here we write the positive and negative instances of the rule
				for rule_instance in rule.positive_instances:
					f.write("%s %s\n" % (POSITIVE_SYMBOL, rule_instance.to_string()))
				for rule_instance in rule.negative_instances:
					f.write("%s %s\n" % (NEGATIVE_SYMBOL, rule_instance.to_string()))

				f.close()


if __name__ == "__main__":

	#configurable parameters
	bench = "/home/facundo/Documents/MySubdominization/fd_subdominization/src/subdominization-training/subdominization_data"
	domain = "caldera"
	runs_folder = os.path.join(bench,domain,"runs/optimal_dirty")
	rules_folder = os.path.join(bench, domain, "rules/conditional_rules")

	run = Run(bench, domain, runs_folder, rules_folder)
	run.write_rules(os.path.join(rules_folder,"all_rules"))

	print("Done.")

