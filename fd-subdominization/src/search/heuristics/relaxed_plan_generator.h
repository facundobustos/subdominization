#ifndef HEURISTICS_RELAXED_PLAN_GENERATOR_H
#define HEURISTICS_RELAXED_PLAN_GENERATOR_H

#include "relaxation_heuristic.h"

#include "../algorithms/priority_queues.h"

#include <vector>

namespace relaxed_plan_generator {

using UnaryOperator = relaxation_heuristic::UnaryOperator;
using Proposition = relaxation_heuristic::Proposition;

class RelaxedPlanGenerator : public relaxation_heuristic::RelaxationHeuristic {
    // Relaxed plans are represented as a set of operators implemented
    // as a bit vector.
    typedef std::vector<bool> RelaxedPlan;
    RelaxedPlan relaxed_plan;

    // for every fact the list of best supporters
    std::vector<std::vector<UnaryOperator *>> reached_by_actions;

    priority_queues::AdaptiveQueue<Proposition *> queue;

    enum PlanType {SINGLE_PLAN, ALL_PLANS};

    bool save_relaxed_plan;
    bool save_relaxed_facts;
    std::string plan_filename;
    std::string facts_filename;
    PlanType plan_type;

    void setup_exploration_queue();
    void setup_exploration_queue_state(const State &state);
    void relaxed_exploration();

    void mark_preferred_operators_and_relaxed_plan(
            const State &state, Proposition *goal);

    void save_relaxed_plan_info_and_terminate(const State &state) const;

    void enqueue_if_necessary(Proposition *prop, int cost, UnaryOperator *op) {
        assert(cost >= 0);
        if (plan_type == ALL_PLANS && prop->cost == cost) {
            reached_by_actions[prop->id].push_back(op);
        }
        if (prop->cost == -1 || prop->cost > cost) {
            prop->cost = cost;
            prop->reached_by = op;
            if (plan_type == ALL_PLANS){
                reached_by_actions[prop->id] = std::vector<UnaryOperator*>(1, op);
            }
            queue.push(cost, prop);
        }
        assert(prop->cost != -1 && prop->cost <= cost);
    }
protected:
    virtual int compute_heuristic(const GlobalState &global_state);
public:
    RelaxedPlanGenerator(const options::Options &options);
    virtual ~RelaxedPlanGenerator() = default;
};
}

#endif
